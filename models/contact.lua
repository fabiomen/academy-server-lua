--------------------------------------------------------------------------------
--
-- Contact Model
--
-- This model is used by the contact form on the website. Users can send us 
-- messages using this form.
--
-- @module contact_model
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------
local val = require "valua"
local contact = {}
-- Attributes and their validation rules
contact.attributes = { 
-- { <attribute> = <validation function, valua required> or "safe" if no validation required for this attribute} 
  { contact_id = "safe" }, -- No validation rules
  { name = val:new().not_empty().len(0,255) }, -- Cannot be empty and must have between 6 and 20 characters
  { email = val:new().not_empty().len(0,255) }, -- Cannot be empty and must have between 6 and 100 characters
  { phone = val:new().len(0,20) }, -- Cannot be empty
  { subject = val:new().not_empty()},
  { msg = val:new().not_empty()}
}

contact.db = {
  key = 'contact_id', -- the primary key
  table = 'contact' -- make sure this field contains the same name as your SQL table!
}

-- @export
return contact