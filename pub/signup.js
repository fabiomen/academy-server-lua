$('#cancel-button').click(function(e) {
    e.preventDefault();
    window.history.back();
});

$('#submit-button').click(function(e) {
    e.preventDefault();
    var email = $("input[name='email']").val()
    var confirm_email = $("input[name='confirm_email']").val()

    if (email !== confirm_email) {
        alert("Os emails não conferem")
    } else {
        $("#inscricao-form").submit();
    }
});