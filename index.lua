local sailor = require "sailor"
local path = require "pl.path"

if path.exists("/home/amoralabs/public_html") then
  path.chdir("/home/amoralabs/public_html")
end

sailor.launch()
