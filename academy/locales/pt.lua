return {
  pt = {
    standard_menu = {
        who_are_we = "Quem Somos",
        contact = "Contato",
        courses = "Cursos",
        login = "Login",
        signup = "Cadastrar",
        locale_en = "English",
        locale_pt = "Português"
    },
    academy = {
        welcome = "Bem vindo a Amora Academy!",
        contact_thanks = "Obrigado pelo contato!",
        login_error = "Erro no login. Erro: ",
        password_label = "Senha",
        confirm_password_label = "Confirmar Senha",
        form_submit_label = "Enviar",
        password_reset_form_title = "Redefinir Senha",
        password_reset_mail_sent = "Enviamos um email para você!",
        password_reset_subject = "[AMORA ACADEMY] Você perdeu sua senha?",
        password_reset_body = [[
            Oi,

            Aqui está o link para resetar sua senha:

            https://amora.academy/academy/password_reset_form/verify/_verify_

            Se você não quiser resetar sua senha ignore este email.

            Abs
            Amora Academy
        ]],
        password_reset_verify_error = "Link incorreto. Por favor, tente novamente",
        password_reset_ok = "Sua senha foi redefinida. Por favor faça <i>login</i> novamente.",
        password_reset_doesnt_match = "Sua senha não está igual ao campo de confirmação... tente novamente!"
    },
    profile = {
        my_account_label = "Minha Conta",
        information_label = "Dados pessoais",
        information_tagline = "Seus dados ué...",
        form_name = "Nome",
        form_email = "E-mail",
        form_password = "Senha",
        form_confirm_password = "Confirmar Senha",
        form_phone = "Telefone",
        courses_label = "Cursos",
        courses_tagline = "Quais cursos você está inscrito(a)",
        forgot_password = "Esqueci minha senha..."

    },
    courses = {
        header = "Cursos",
        tag_line = "Que tal aprender mais sobre IoT? Criação de Jogos? A Web?",
        learn_more = "Saiba mais",
        enroll = "Inscreva-se",
        resume_learning = "Abrir"
    },
    footer = {
        tag_line = [[
            <strong>Amora Academy</strong> por <a href="http://amoralabs.com">Amora Labs</a>. Por uma educação fora da caixinha.
        ]],
        sailor_line = [[
            Feito com <i class="fa fa-heart-o"></i> usando
            <a href="http://sailorproject.org" target="_blank">Sailor</a> e
            <a href="http://www.lua.org" target="_blank">Lua</a>.
        ]]
    },
    unit = {
        beta_notice_header = "Aviso: Conteúdo BETA",
        beta_notice = [[
            Esse conteúdo é <strong>BETA</strong>. Ele está sendo compartilhado com a comunidade para ser testado e para coletarmos <i>feedback</i>. Por favor, conte para gente o que achou dele e fique ciente que este conteúdo provavelmente mudará antes de sair do beta.
        ]],
        continue = "Continuar"
    }
  }
}
