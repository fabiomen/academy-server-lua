<!--
title: "Amora Academy"
subtitle: "Educação fora da caixinha!"
-->
A **Amora Academy** é a plataforma de cursos online da **Amora Labs**, uma empresa focada na criação de novos recursos educacionais para desenvolvedores, makers e curiosos. Oferecemos cursos gratuitos acessíveis para todos e cursos *premium* exclusivos para nossos assinantes.

## Cursos gratuitos
Acreditamos que o acesso à materiais educacionais de qualidade e gratuitos sobre desenvolvimento é algo fundamental. Uma pessoa não deve ser impedida de aprender apenas por não ter recursos financeiros suficientes. Por isso, todo o nosso material básico é disponibilizado sem custo aqui na **Amora Academy**.

## Cursos premium
Esse material irá além do básico e está disponível em forma de uma assinatura super acessível. **Por R$ 30,00 mensais você terá acesso a todo o nosso catálogo de cursos premium e, de quebra, ainda ajuda a manter o site.** 