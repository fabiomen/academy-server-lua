--------------------------------------------------------------------------------
--
-- Courses Controller
--
-- This controller is the "course taking" code which drives all our courses. it
-- makes use of [markdown](https://luarocks.org/modules/soapdog/lua-markdown-extra)
-- and [ini](http://stevedonovan.github.io/Penlight/api/libraries/pl.config.html)
-- files for the course content.
--
-- @module courses_controller
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------

local courses = {}
local utils = require 'academy.utils'

--- the main courses page
-- this is function is called when the user access the list of courses on the website.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.index(page)
  page:render("index", {courses = utils.available_courses()})
end

--- page about a given course
-- this is function is called when the user requests information about a given course.
-- The course param comes from the GET request.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.about(page)
  -- mostra a landing page de um curso específico
  local course = utils.course(page.GET.course)

  if course == nil then
    return 404
  end

  page:render("about", {course = course})
end

--- course unit visualization
-- this is function is called when the user is taking a course, it displays a given unit.
-- The course params comes from the GET request.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.v(page)
  local course = page.GET.course
  local unit = page.GET.unit
  local item = page.GET.item
  local obj = utils.item(course, unit, item)
  local user = utils.logged_user()

  if course == nil or unit == nil then
    return 404
  end

  if not user then
    page:redirect("academy/login_form/")
  end

  if not user:is_enrolled(obj.course) then
    page:redirect("courses/about/course/" .. course)
  end

  local ok, err = user:progress(obj)

  if not ok then
    utils.log("error in progress: " .. err)
  end

  page.layout = "reader"
  page:render("unit", obj)
end

--- Enroll in course
-- enrolls the logged use in a given course.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.enroll(page)
  local course = page.GET.course
  local user = utils.logged_user()

  if not course then
    return 404
  end

  course = utils.course(course)

  if not user then
    page:redirect("academy/login_form/")
  end

  local ok,err = user:enroll(course)

  if ok then
    page:redirect("courses/v/course/" .. course.config.id .. "/unit/1/item/1")
  else
    local pretty = require "pl.pretty"
    utils.error("error: " .. pretty.write(err))
    return 404
  end
end

--- Resume learning
-- Fetches progress and resume learning from the latest item.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.resume(page)
  local course = page.GET.course
  local user = utils.logged_user()

  if not user then
    page:redirect("academy/login_form/")
  end

  if not course then
    return 404
  end

  course = utils.course(course)

  local is_enrolled = user:is_enrolled(course)

  if not is_enrolled then
    page:redirect("courses/about/course" .. course.config.id)
  end

  local current_progress = user:furthest_item_for_course(course)


  if current_progress then
    page:redirect("courses/v/course/" .. course.config.id .. "/unit/"..current_progress.furthest_unit.."/item/" ..current_progress.furthest_item)
  else
    return 404
  end
end

--- advance to next item
-- this redirects the user to the next item in their current course.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function courses.advance_to_next_item(page)
  local course = page.GET.course
  local unit = page.GET.unit
  local item = page.GET.item
  local user = utils.logged_user()

  -- boilerplate to check if we have the params we need.

  if not user then
    page:redirect("academy/view/page/login")
  end

  if not course or not unit or not item then
    return 404
  end

  course = utils.course(course)

  local is_enrolled = user:is_enrolled(course)

  if not is_enrolled then
    page:redirect("courses/about/course" .. course.config.id)
  end

  -- find the next item...
  local number_of_items_in_unit = #course["unit" .. tostring(unit)].toc
  local is_units_end = (number_of_items_in_unit == tonumber(item))

  if is_units_end then
    unit = unit + 1
    item = 1
  else
    item = item + 1
  end

  -- redirect user to the next item
  local url = string.format("courses/v/course/%s/unit/%d/item/%d", course.config.id, unit, item)
  page:redirect(url)
end

-- @export
return courses
