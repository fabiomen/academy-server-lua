return {
  en = {
    standard_menu = {
        who_are_we = "Who are we?",
        contact = "Contact",
        courses = "Courses",
        login = "Login",
        signup = "Sign Up",
        locale_en = "English",
        locale_pt = "Português"
    },
    academy = {
        welcome = "Welcome to Amora Academy!",
        contact_thanks = "Thank you for your message!",
        login_error = "Login Error: ",
        password_label = "Password",
        confirm_password_label = "Confirm Password",
        form_submit_label = "Submit",
        password_reset_form_title = "Reset Password",
        password_reset_mail_sent = "We have sent you an email!",
        password_reset_subject = "[AMORA ACADEMY] Did you forgot your password?",
        password_reset_body = [[
            Hi,

            Here is the link to reset your password:

            https://amora.academy/academy/password_reset_form/verify/_verify_

            If you don't want to reset your password, please ignore this email.

            Om om
            Amora Academy
        ]],
        password_reset_verify_error = "Wrong link, please try again",
        password_reset_ok = "Your new password was saved. Please <i>login</i> again.",
        password_reset_doesnt_match = "Your password and its confirmation do not match. Try again!"

    },
    profile = {
        my_account_label = "My Account",
        information_label = "Personal Info",
        information_tagline = "This is your data",
        form_name = "Name",
        form_email = "E-mail",
        form_password = "Password",
        form_confirm_password = "Confirm Password",
        form_phone = "Phone",
        courses_label = "Courses",
        courses_tagline = "What courses are you enrolled",
        forgot_password = "Ooops, forgot my password"
    },
    courses = {
        header = "Courses",
        tag_line = "How about learning more about IoT? Game Development? The Web?",
        learn_more = "Learn More",
        enroll = "Enroll",
        resume_learning = "Resume Learning"
    },
    footer = {
        tag_line = [[
            <strong>Amora Academy</strong> by <a href="http://amoralabs.com">Amora Labs</a>. For an out-of-the-box education.
        ]],
        sailor_line = [[
            Made with <i class="fa fa-heart-o"></i> using
            <a href="http://sailorproject.org" target="_blank">Sailor</a> and
            <a href="http://www.lua.org" target="_blank">Lua</a>
        ]]
    },
    unit = {
        beta_notice_header = "Notice: BETA Content",
        beta_notice = [[
            This content is <strong>BETA</strong>. It is being shared with the community for testing and early feedback. Please reach out for us with your impressions and comments and be aware that this content might (and probably will) change before the beta is over.
        ]],
        continue = "Continue"
    }
  }
}
