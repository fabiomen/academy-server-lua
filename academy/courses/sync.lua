#!env lua
--- sync.lua
--
-- Reads an the general .ini file and check out the repos.
--
-- @script readconfig.lua
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--
-- @usage sync.lua curso-blablabla/course.ini
-- @param path, the file to read.
--
require("socket")
local config = require 'pl.config'
local pretty = require 'pl.pretty'
local colors = require 'ansicolors'
local json = require "dkjson"
local https = require "ssl.https"
local path = require "pl.path"

local config_file = 'courses.ini' 
local c = config.read(config_file)

print(colors("%{magentabg}... AMORA SYNC ...%{reset}"))
print(colors("%{blink bluebg}Checando repositórios... %{reset}"))

for i,r in ipairs(c.config.cursos) do
    local repo = c.config.github_org .. "/" .. r
    local repo_check_url = "https://api.github.com/repos/" .. repo

    -- checar se existe
    print(colors("%{blue}Checando: " .. repo .. "%{reset}"))
    local body, code, headers, status = https.request(repo_check_url)
    local data = json.decode(body)

    if code == 404 then
        print(colors("%{red}" .. repo .. " Não encontrado %{reset}"))
    else
        -- se o diretorio já existir, update
        if path.exists(r) then
            print(colors("%{green}" .. repo .. " Update... %{reset}"))
        else
        -- se não existir clone
            print(colors("%{green bright}" .. repo .. " Clonando... %{reset}"))
        end
    end

end


