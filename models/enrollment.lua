--------------------------------------------------------------------------------
--
-- Enrollment Model
--
-- This model represents a users enrollment in a given course.
--
-- @module enrollment_model
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------
local val = require "valua"

local enrollment = {}
-- Attributes and their validation rules
enrollment.attributes = { 
  { enrollment_id = "safe" }, -- No validation rules
  { user_id = val:new().not_empty().integer() },
  { course = val:new().not_empty().len(0,255) }, 
  { date = "safe"}
}

enrollment.db = {
  key = 'enrollment_id', -- the primary key
  table = 'enrollment' -- make sure this field contains the same name as your SQL table!
}

enrollment.relations = {
  user = {relation = "HAS_ONE", model = "user", attribute = "user_id"}
}

return enrollment